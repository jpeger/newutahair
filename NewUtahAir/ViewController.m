//
//  ViewController.m
//  NewUtahAir
//
//  Created by Developement on 8/31/14.
//  Copyright (c) 2014 NCAST. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize menuDrawerWidth,menuDrawerX,recognizer_close,recognizer_open;

#pragma mark util functions

- (UIActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    }
    return _indicator;
}

-(NSManagedObjectContext*)managedObjectContext{
    NSManagedObjectContext* contex = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        contex = [delegate managedObjectContext];
    }
    return contex;
}

#pragma mark view functions

- (void)viewDidLoad
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    selectedCounties = [[defaults arrayForKey:@"selectedCounties"]mutableCopy];
    
    [super viewDidLoad];
    
    NSNumber* value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice]setValue:value forKey:@"orientation"];
    
    //check network status
    networkReachability = [Reachability reachabilityForInternetConnection];
    networkStatus = [networkReachability currentReachabilityStatus];
    
    
    float height = self.view.frame.size.height;
    float width = self.view.frame.size.width;
    
    polutant = @"pm25";
    
    //get current date and time then format
    NSDate* now = [NSDate date];
    NSDateFormatter* formatNow = [[NSDateFormatter alloc]init];
    
    [formatNow setDateFormat:@"MM/dd/yy HH:mm"];
    
    //add navigation title
    self.navigationItem.title = @"Utah Air";
    
    //add backround image
    UIImage* backgrounImage = [UIImage imageNamed:@"background_iphone1.png"];
    background = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [background setImage:backgrounImage];
    [background setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:background];
    
    //menu bar
    UIBarButtonItem* favoritesList = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(showFavoritesMenu:)];
    UIBarButtonItem* spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* pickPollutant = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Cloud-icon"] style:UIBarButtonItemStyleDone target:self action:@selector(showPollutantOptions:)];
    UIBarButtonItem* refresh = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshDATA)];
    dropDown = [[UIBarButtonItem alloc]initWithTitle:@"a" style:UIBarButtonItemStylePlain target:self action:@selector(showMenuOptions:)];
    [dropDown setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"menu" size:30.0], NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateNormal];
    [dropDown setTitlePositionAdjustment:UIOffsetMake(0, 5.0) forBarMetrics:UIBarMetricsDefault];
    NSArray* navigationMenuItems = [NSArray arrayWithObjects:dropDown, refresh, pickPollutant, spacer,nil];
    self.navigationItem.rightBarButtonItems = navigationMenuItems;
    self.navigationItem.leftBarButtonItem = favoritesList;
    
    //[self.view addSubview:menu];
    
    //pm 2.5 label
    pm = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, (width/2)-10, height/25)];
    [pm setText:@"PM 2.5"];
    pm.textColor = [UIColor whiteColor];
    [pm setFont:[UIFont boldSystemFontOfSize:17]];
    pm.layer.shadowColor = [[UIColor blackColor] CGColor];
    pm.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    pm.layer.shadowOpacity = 1.0f;
    pm.layer.shadowRadius = 1.0f;
    pm.shadowColor = [UIColor blackColor];
    pm.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:pm];
    
    //date time label
    dateTime =[[UILabel alloc]initWithFrame:CGRectMake(pm.frame.origin.x + pm.frame.size.width, pm.frame.origin.y, pm.frame.size.width, pm.frame.size.height)];
    dateTime.textColor = [UIColor whiteColor];
    [dateTime setTextAlignment:NSTextAlignmentRight];
    [dateTime setText:[formatNow stringFromDate:now]];
    [dateTime setFont:[UIFont boldSystemFontOfSize:17]];
    dateTime.layer.shadowColor = [[UIColor blackColor] CGColor];
    dateTime.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    dateTime.layer.shadowOpacity = 1.0f;
    dateTime.layer.shadowRadius = 1.0f;
    dateTime.shadowColor = [UIColor blackColor];
    dateTime.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:dateTime];
    
    //location name label
    location = [[UILabel alloc]initWithFrame:CGRectMake(10, pm.frame.origin.y+pm.frame.size.height, width-20, pm.frame.size.height)];
    location.textColor = [UIColor whiteColor];
    [location setTextAlignment:NSTextAlignmentCenter];
    [location setText:@""];
    [location setFont:[UIFont boldSystemFontOfSize:17]];
    location.layer.shadowColor = [[UIColor blackColor] CGColor];
    location.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    location.layer.shadowOpacity = 1.0f;
    location.layer.shadowRadius = 1.0f;
    location.shadowColor = [UIColor blackColor];
    location.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:location];
    
    //pm 2.5 count
    pmCount = [[UILabel alloc]initWithFrame:CGRectMake(width/7, location.frame.origin.y+location.frame.size.height, (width/2)-10, height/8)];
    pmCount.textColor = [UIColor grayColor];
    [pmCount setText:@""];
    [pmCount setTextAlignment:NSTextAlignmentRight];
    [pmCount setFont:[UIFont boldSystemFontOfSize:50]];
    pmCount.layer.shadowColor = [[UIColor blackColor] CGColor];
    pmCount.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    pmCount.layer.shadowOpacity = 1.0f;
    pmCount.layer.shadowRadius = 1.0f;
    pmCount.shadowColor = [UIColor blackColor];
    pmCount.shadowOffset = CGSizeMake(1.0f, 1.0f);
    [self.view addSubview:pmCount];
    
    //pm 2.5 trending arrow
    UIImage* arrow = [UIImage imageNamed:@""];
    trendArrow = [[UIImageView alloc]initWithFrame:CGRectMake(pmCount.frame.origin.x+pmCount.frame.size.width+10, pmCount.frame.origin.y+pmCount.frame.size.height/3 - 7, pmCount.frame.size.height/2, pmCount.frame.size.height/2)];
    [trendArrow setImage:arrow];
    [trendArrow setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:trendArrow];
    
    //poultion range label
    pollutantRange = [[UILabel alloc]initWithFrame:CGRectMake(10, pmCount.frame.origin.y+pmCount.frame.size.height, width-20, height/25)];
    pollutantRange.textColor = [UIColor grayColor];
    [pollutantRange setTextAlignment:NSTextAlignmentCenter];
    [pollutantRange setText:@""];
    [pollutantRange setFont:[UIFont boldSystemFontOfSize:17]];
    pollutantRange.layer.shadowColor = [[UIColor blackColor] CGColor];
    pollutantRange.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    pollutantRange.layer.shadowOpacity = 1.0f;
    pollutantRange.layer.shadowRadius = 1.0f;
    pollutantRange.shadowColor = [UIColor blackColor];
    pollutantRange.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:pollutantRange];
    
    //unit label
    pollutionUnit = [[UILabel alloc]initWithFrame:CGRectMake(10, pollutantRange.frame.origin.y+pollutantRange.frame.size.height, width-20, height/30)];
    pollutionUnit.textColor = [UIColor grayColor];
    [pollutionUnit setTextAlignment:NSTextAlignmentCenter];
    [pollutionUnit setText:@""];
    [pollutionUnit setFont:[UIFont boldSystemFontOfSize:17]];
    pollutionUnit.layer.shadowColor = [[UIColor blackColor] CGColor];
    pollutionUnit.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    pollutionUnit.layer.shadowOpacity = 1.0f;
    pollutionUnit.layer.shadowRadius = 1.0f;
    pollutionUnit.shadowColor = [UIColor blackColor];
    pollutionUnit.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:pollutionUnit];
    
    //temperature label
    UILabel* temperatureLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, pollutionUnit.frame.origin.y+pollutionUnit.frame.size.height, (width/2)-10, height/30)];
    [temperatureLbl setTextColor:[UIColor whiteColor]];
    [temperatureLbl setTextAlignment:NSTextAlignmentCenter];
    [temperatureLbl setText:@"Temperature:"];
    [temperatureLbl setFont:[UIFont boldSystemFontOfSize:17]];
    temperatureLbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    temperatureLbl.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    temperatureLbl.layer.shadowOpacity = 1.0f;
    temperatureLbl.layer.shadowRadius = 1.0f;
    temperatureLbl.shadowColor = [UIColor blackColor];
    temperatureLbl.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:temperatureLbl];
    
    //wind label
    UILabel* windLbl = [[UILabel alloc]initWithFrame:CGRectMake(temperatureLbl.frame.origin.x+temperatureLbl.frame.size.width, temperatureLbl.frame.origin.y, temperatureLbl.frame.size.width, temperatureLbl.frame.size.height)];
    [windLbl setTextColor:[UIColor whiteColor]];
    [windLbl setTextAlignment:NSTextAlignmentCenter];
    [windLbl setText:@"Wind:"];
    [windLbl setFont:[UIFont boldSystemFontOfSize:17]];
    windLbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    windLbl.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    windLbl.layer.shadowOpacity = 1.0f;
    windLbl.layer.shadowRadius = 1.0f;
    windLbl.shadowColor = [UIColor blackColor];
    windLbl.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:windLbl];
    
    //temperature value
    temperatureValue = [[UILabel alloc]initWithFrame:CGRectMake(10, temperatureLbl.frame.origin.y+temperatureLbl.frame.size.height, temperatureLbl.frame.size.width, temperatureLbl.frame.size.height)];
    [temperatureValue setTextColor:[UIColor whiteColor]];
    [temperatureValue setTextAlignment:NSTextAlignmentCenter];
    [temperatureValue setText:@""];
    [temperatureValue setFont:[UIFont boldSystemFontOfSize:17]];
    temperatureValue.layer.shadowColor = [[UIColor blackColor] CGColor];
    temperatureValue.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    temperatureValue.layer.shadowOpacity = 1.0f;
    temperatureValue.layer.shadowRadius = 1.0f;
    temperatureValue.shadowColor = [UIColor blackColor];
    temperatureValue.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:temperatureValue];
    
    //wind value
    windValue = [[UILabel alloc]initWithFrame:CGRectMake(temperatureValue.frame.origin.x+temperatureValue.frame.size.width, temperatureValue.frame.origin.y, temperatureValue.frame.size.width, temperatureValue.frame.size.height)];
    [windValue setTextColor:[UIColor whiteColor]];
    [windValue setTextAlignment:NSTextAlignmentCenter];
    [windValue setText:@""];
    [windValue setFont:[UIFont boldSystemFontOfSize:17]];
    windValue.layer.shadowColor = [[UIColor blackColor] CGColor];
    windValue.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    windValue.layer.shadowOpacity = 1.0f;
    windValue.layer.shadowRadius = 1.0f;
    windValue.shadowColor = [UIColor blackColor];
    windValue.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:windValue];
    
    //set up variables for health forcast
    int healthWidth = (width - (width/5))/3;
    
    //today forcast label
    todayForcastLbl = [[UILabel alloc]initWithFrame:CGRectMake(width/10, height*0.50, healthWidth, height/30)];
    [todayForcastLbl setTextColor:[UIColor whiteColor]];
    [todayForcastLbl setTextAlignment:NSTextAlignmentCenter];
    [todayForcastLbl setText:@""];
    [todayForcastLbl setFont:[UIFont boldSystemFontOfSize:15]];
    todayForcastLbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    todayForcastLbl.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    todayForcastLbl.layer.shadowOpacity = 1.0f;
    todayForcastLbl.layer.shadowRadius = 1.0f;
    todayForcastLbl.shadowColor = [UIColor blackColor];
    todayForcastLbl.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:todayForcastLbl];
    
    //tomorrow forcast label
    tomorrowForcastLbl = [[UILabel alloc]initWithFrame:CGRectMake(todayForcastLbl.frame.origin.x+healthWidth, height*0.50, healthWidth, height/30)];
    [tomorrowForcastLbl setTextColor:[UIColor whiteColor]];
    [tomorrowForcastLbl setTextAlignment:NSTextAlignmentCenter];
    [tomorrowForcastLbl setText:@""];
    [tomorrowForcastLbl setFont:[UIFont boldSystemFontOfSize:15]];
    tomorrowForcastLbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    tomorrowForcastLbl.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    tomorrowForcastLbl.layer.shadowOpacity = 1.0f;
    tomorrowForcastLbl.layer.shadowRadius = 1.0f;
    tomorrowForcastLbl.shadowColor = [UIColor blackColor];
    tomorrowForcastLbl.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:tomorrowForcastLbl];
    
    //nextday forcast label
    nextdayForcastLbl = [[UILabel alloc]initWithFrame:CGRectMake(tomorrowForcastLbl.frame.origin.x+healthWidth, height*0.50, healthWidth, height/30)];
    [nextdayForcastLbl setTextColor:[UIColor whiteColor]];
    [nextdayForcastLbl setTextAlignment:NSTextAlignmentCenter];
    [nextdayForcastLbl setText:@""];
    [nextdayForcastLbl setFont:[UIFont boldSystemFontOfSize:15]];
    nextdayForcastLbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    nextdayForcastLbl.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    nextdayForcastLbl.layer.shadowOpacity = 1.0f;
    nextdayForcastLbl.layer.shadowRadius = 1.0f;
    nextdayForcastLbl.shadowColor = [UIColor blackColor];
    nextdayForcastLbl.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:nextdayForcastLbl];
    
    //today health image
    UIImage* todayHealthImage = [UIImage imageNamed:@""];
    todayHealthIMGView = [[UIImageView alloc]initWithFrame:CGRectMake(todayForcastLbl.frame.origin.x+5, todayForcastLbl.frame.origin.y+todayForcastLbl.frame.size.height, healthWidth-10, height/10)];
    [todayHealthIMGView setImage:todayHealthImage];
    [todayHealthIMGView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:todayHealthIMGView];
    
    //tomorrow health image
    UIImage* tomorrowHealthImage = [UIImage imageNamed:@""];
    tomorrowHealthIMGView = [[UIImageView alloc]initWithFrame:CGRectMake(tomorrowForcastLbl.frame.origin.x+5, tomorrowForcastLbl.frame.origin.y+tomorrowForcastLbl.frame.size.height, healthWidth-10, height/10)];
    [tomorrowHealthIMGView setImage:tomorrowHealthImage];
    [tomorrowHealthIMGView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:tomorrowHealthIMGView];
    
    //nextday health image
    UIImage* nextdayHealthImage = [UIImage imageNamed:@""];
    nextdayHealthIMGView = [[UIImageView alloc]initWithFrame:CGRectMake(nextdayForcastLbl.frame.origin.x+5, nextdayForcastLbl.frame.origin.y+nextdayForcastLbl.frame.size.height, healthWidth-10, height/10)];
    [nextdayHealthIMGView setImage:nextdayHealthImage];
    [nextdayHealthIMGView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:nextdayHealthIMGView];
    
    //today health label
    todayHealthLbl = [[UITextView alloc]initWithFrame:CGRectMake(todayHealthIMGView.frame.origin.x, todayHealthIMGView.frame.origin.y, todayHealthIMGView.frame.size.width, todayHealthIMGView.frame.size.height)];
    [todayHealthLbl setTextColor:[UIColor blackColor]];
    [todayHealthLbl setTextAlignment:NSTextAlignmentCenter];
    [todayHealthLbl setText:@""];
    [todayHealthLbl setBackgroundColor:[UIColor clearColor]];
    [todayHealthLbl setUserInteractionEnabled:NO];
    [self.view addSubview:todayHealthLbl];
    
    //tomorrow health label
    tomorrowHealthLbl = [[UITextView alloc]initWithFrame:CGRectMake(tomorrowHealthIMGView.frame.origin.x, tomorrowHealthIMGView.frame.origin.y, tomorrowHealthIMGView.frame.size.width, tomorrowHealthIMGView.frame.size.height)];
    [tomorrowHealthLbl setTextColor:[UIColor blackColor]];
    [tomorrowHealthLbl setTextAlignment:NSTextAlignmentCenter];
    [tomorrowHealthLbl setText:@""];
    [tomorrowHealthLbl setBackgroundColor:[UIColor clearColor]];
    [tomorrowHealthLbl setUserInteractionEnabled:NO];
    
    [self.view addSubview:tomorrowHealthLbl];
    
    //nextday health label
    nextdayHealthLbl = [[UITextView alloc]initWithFrame:CGRectMake(nextdayHealthIMGView.frame.origin.x, nextdayHealthIMGView.frame.origin.y, nextdayHealthIMGView.frame.size.width, nextdayHealthIMGView.frame.size.height)];
    [nextdayHealthLbl setTextColor:[UIColor blackColor]];
    [nextdayHealthLbl setTextAlignment:NSTextAlignmentCenter];
    [nextdayHealthLbl setText:@""];
    [nextdayHealthLbl setBackgroundColor:[UIColor clearColor]];
    [nextdayHealthLbl setUserInteractionEnabled:NO];
    [self.view addSubview:nextdayHealthLbl];
    
    //today burn condition img
    UIImage* todayBurnImg = [UIImage imageNamed:@""];
    todayBurnImgView = [[UIImageView alloc]initWithFrame:CGRectMake(todayHealthIMGView.frame.origin.x, todayHealthIMGView.frame.origin.y+todayHealthIMGView.frame.size.height, todayHealthIMGView.frame.size.width, todayHealthIMGView.frame.size.width)];
    [todayBurnImgView setImage:todayBurnImg];
    [todayBurnImgView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:todayBurnImgView];
    
    //tomorrow burn condition img
    UIImage* tomorrowBurnImg = [UIImage imageNamed:@""];
    tomorrowBurnImgView = [[UIImageView alloc]initWithFrame:CGRectMake(tomorrowHealthIMGView.frame.origin.x, tomorrowHealthIMGView.frame.origin.y+tomorrowHealthIMGView.frame.size.height, tomorrowHealthIMGView.frame.size.width, tomorrowHealthIMGView.frame.size.width)];
    [tomorrowBurnImgView setImage:tomorrowBurnImg];
    [tomorrowBurnImgView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:tomorrowBurnImgView];
    
    //nextday burn condition img
    UIImage* nextdayBurnImg = [UIImage imageNamed:@""];
    nextdayBurnImgView = [[UIImageView alloc]initWithFrame:CGRectMake(nextdayHealthIMGView.frame.origin.x, nextdayHealthIMGView.frame.origin.y+nextdayHealthIMGView.frame.size.height, nextdayHealthIMGView.frame.size.width, nextdayHealthIMGView.frame.size.width)];
    [nextdayBurnImgView setImage:nextdayBurnImg];
    [nextdayBurnImgView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:nextdayBurnImgView];
    
    //today burn label
    todayBurnlbl = [[UILabel alloc]initWithFrame:CGRectMake(todayBurnImgView.frame.origin.x, todayBurnImgView.frame.origin.y+todayBurnImgView.frame.size.height, todayBurnImgView.frame.size.width, height/25)];
    [todayBurnlbl setTextColor:[UIColor whiteColor]];
    [todayBurnlbl setTextAlignment:NSTextAlignmentCenter];
    [todayBurnlbl setText:@""];
    [todayBurnlbl setFont:[UIFont boldSystemFontOfSize:17]];
    todayBurnlbl.adjustsFontSizeToFitWidth=YES;
    todayBurnlbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    todayBurnlbl.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    todayBurnlbl.layer.shadowOpacity = 1.0f;
    todayBurnlbl.layer.shadowRadius = 1.0f;
    todayBurnlbl.shadowColor = [UIColor blackColor];
    todayBurnlbl.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:todayBurnlbl];
    
    //tomorrow burn label
    tomorrowBurnlbl = [[UILabel alloc]initWithFrame:CGRectMake(tomorrowBurnImgView.frame.origin.x, tomorrowBurnImgView.frame.origin.y+tomorrowBurnImgView.frame.size.height, tomorrowBurnImgView.frame.size.width, height/25)];
    [tomorrowBurnlbl setTextColor:[UIColor whiteColor]];
    [tomorrowBurnlbl setTextAlignment:NSTextAlignmentCenter];
    [tomorrowBurnlbl setText:@""];
    [tomorrowBurnlbl setFont:[UIFont boldSystemFontOfSize:17]];
    tomorrowBurnlbl.adjustsFontSizeToFitWidth=YES;
    tomorrowBurnlbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    tomorrowBurnlbl.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    tomorrowBurnlbl.layer.shadowOpacity = 1.0f;
    tomorrowBurnlbl.layer.shadowRadius = 1.0f;
    tomorrowBurnlbl.shadowColor = [UIColor blackColor];
    tomorrowBurnlbl.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:tomorrowBurnlbl];
    
    //nextday burn label
    nextdayBurnlbl = [[UILabel alloc]initWithFrame:CGRectMake(nextdayBurnImgView.frame.origin.x, nextdayBurnImgView.frame.origin.y+nextdayBurnImgView.frame.size.height, nextdayBurnImgView.frame.size.width, height/25)];
    [nextdayBurnlbl setTextColor:[UIColor whiteColor]];
    [nextdayBurnlbl setTextAlignment:NSTextAlignmentCenter];
    [nextdayBurnlbl setText:@""];
    [nextdayBurnlbl setFont:[UIFont boldSystemFontOfSize:17]];
    nextdayBurnlbl.adjustsFontSizeToFitWidth=YES;
    nextdayBurnlbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    nextdayBurnlbl.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    nextdayBurnlbl.layer.shadowOpacity = 1.0f;
    nextdayBurnlbl.layer.shadowRadius = 1.0f;
    nextdayBurnlbl.shadowColor = [UIColor blackColor];
    nextdayBurnlbl.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:nextdayBurnlbl];
    
    //submenu and submenu items
    menuDrawerWidth = self.view.frame.size.width*0.75;
    menuDrawerX = self.view.frame.origin.x - menuDrawerWidth;
    menuDrawer = [[UITableView alloc]initWithFrame:CGRectMake(menuDrawerX, menu.frame.size.height, menuDrawerWidth, height-menu.frame.size.height-65)];
    [menuDrawer setDataSource:self];
    [menuDrawer setDelegate:self];
    
    [menuDrawer setBackgroundColor:[UIColor darkGrayColor]];
    
    recognizer_close = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipes:)];
    recognizer_open = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipes:)];
    
    [recognizer_close setDirection:UISwipeGestureRecognizerDirectionLeft];
    [recognizer_open setDirection:UISwipeGestureRecognizerDirectionRight];
    
    self.indicator.center = self.view.center;
    [self.view addSubview:self.indicator];
    
    [self.view addGestureRecognizer:recognizer_open];
    [self.view addGestureRecognizer:recognizer_close];
    
    [self.view addSubview:menuDrawer];
    
    
    [self registerForNotifications];
}

-(void)registerForNotifications{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recievedCounties:) name:@"SelectedCounties" object:nil];
}

-(void)recievedCounties:(NSNotification*) notification{
    selectedCounties = [notification object];
    
    NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
    //NSLog(@"here%@", counties);
    countyData = [[NSMutableArray alloc]init];
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Station"];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"stationID IN %@", selectedCounties];
    [fetchRequest setPredicate:predicate];
    NSError* error = nil;
    
    NSSortDescriptor* mainDesctiptor = [NSSortDescriptor sortDescriptorWithKey:@"label" ascending:YES];
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [fetchRequest setSortDescriptors:@[mainDesctiptor,sortDescriptor]];
    
    countyData = [[managedObjectContext executeFetchRequest:fetchRequest error:&error]mutableCopy];
    //NSLog(@"Station ID: %@",[countyData valueForKey:@"stationID"]);
    if (countyData.count > 0) {
        currentSelection = countyData[0];
    }
    [self redrawData];
    [menuDrawer reloadData];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSNumber* value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice]setValue:value forKey:@"orientation"];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!selectedCounties || selectedCounties.count < 1) {
        [self goToRegions];
    }else{
        [_indicator startAnimating];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSManagedObjectContext* managedObjectContext = [self managedObjectContext];

            countyData = [[NSMutableArray alloc]init];
            NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Station"];
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"stationID IN %@", selectedCounties];
            [fetchRequest setPredicate:predicate];
            NSError* error = nil;
            
            NSSortDescriptor* mainDesctiptor = [NSSortDescriptor sortDescriptorWithKey:@"label" ascending:YES];
            NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
            [fetchRequest setSortDescriptors:@[mainDesctiptor,sortDescriptor]];
            
            countyData = [[managedObjectContext executeFetchRequest:fetchRequest error:&error]mutableCopy];
            

            if (countyData.count > 0) {
                NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                int last = (int)[[defaults valueForKey:@"lastSelectedSite"] integerValue];
                currentSelection = countyData[last];
                polutant = [currentSelection valueForKey:@"seasonalParameter"];
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                [self refreshDATA];
                [_indicator stopAnimating];
                [self redrawData];
                [menuDrawer reloadData];
            });
        });
        
        
    }
    NSNumber* value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice]setValue:value forKey:@"orientation"];
}

#pragma mark menu controlls

-(IBAction)showMenuOptions:(id)sender{
    UIActionSheet* subMenu = [[UIActionSheet alloc]initWithTitle:@"Navigate To:" delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Favorites", @"Trend Chart", @"Forecast Legend", @"Disclaimer", @"About", nil];
    
    [subMenu showFromBarButtonItem:dropDown animated:YES];
}

-(IBAction)showPollutantOptions:(id)sender{
    UIActionSheet* pollutantSelect = [[UIActionSheet alloc]initWithTitle:@"Select Pollutant" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"PM 2.5", @"Ozone", nil];
    
    [pollutantSelect showFromBarButtonItem:dropDown animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString* buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([buttonTitle isEqualToString:@"Favorites"]) {
        [self goToRegions];
    }
    if ([buttonTitle isEqualToString:@"Trend Chart"]) {
        [self goToTrendChart];
    }
    if ([buttonTitle isEqualToString:@"Forecast Legend"]) {
        [self goToLegend];
    }
    if ([buttonTitle isEqualToString:@"Advisory"]) {
        [self goToAdvisory];
    }
    if ([buttonTitle isEqualToString:@"Disclaimer"]) {
        [self goToDisclaimer];
    }
    if ([buttonTitle isEqualToString:@"About"]) {
        [self goToAbout];
    }
    if ([buttonTitle isEqualToString:@"PM 2.5"]) {
        polutant = @"pm25";
        [pm setText:@"PM 2.5"];
        [self redrawData];
    }
    if ([buttonTitle isEqualToString:@"Ozone"]) {
        polutant = @"ozone";
        [pm setText:@"Ozone"];
        [self redrawData];
    }
}

//dispaly and hide menu
-(void)drawAnimation{
    [UITableView beginAnimations:nil context:nil];
    [UITableView setAnimationDelegate:self];
    [UITableView setAnimationDuration:-5];
    
    CGFloat new_x = 0;
    if (menuDrawer.frame.origin.x < 0) {
        new_x = menuDrawer.frame.origin.x+menuDrawerWidth;
    } else {
        new_x = menuDrawer.frame.origin.x-menuDrawerWidth;
    }
    
    menuDrawer.frame = CGRectMake(new_x, menuDrawer.frame.origin.y, menuDrawer.frame.size.width, menuDrawer.frame.size.height);
    [UITableView commitAnimations];
}

-(void)handleSwipes:(UISwipeGestureRecognizer *)sender{
    [self drawAnimation];
}

-(IBAction)showFavoritesMenu:(id)sender{
    [self drawAnimation];
}

#pragma mark navigation functions

-(void)goToRegions{
    [_indicator startAnimating];
    RegionsViewController* regionsView = [[RegionsViewController alloc]init];
    networkStatus = [networkReachability currentReachabilityStatus];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        if (networkStatus == NotReachable) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"RegionsDictionary" object:nil];
        } else {
            //get region data
            NSURL* url = [[NSURL alloc]initWithString:@"http://air.utah.gov/phoneApp.php?p=region"];
            NSURLRequest* regionRequest = [NSURLRequest requestWithURL:url];
            NSURLResponse* response = nil;
            NSError* err = nil;
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
            NSData* data = [NSURLConnection sendSynchronousRequest:regionRequest returningResponse:&response error:&err];
            
            NSDictionary* regions = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSMutableArray* regionList = [[regions[@"stationList"] allKeys]mutableCopy];
            NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
            NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Region"];
            regionData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            //no region data found
            if (regionData == nil || regionData.count < 1) {
                for (int i = 0; i<regionList.count; i++) {
                    NSData* stationIDS = [NSKeyedArchiver archivedDataWithRootObject:regions[@"stationList"][regionList[i]]];
                    NSManagedObject* newRegion = [NSEntityDescription insertNewObjectForEntityForName:@"Region" inManagedObjectContext:managedObjectContext];
                    [newRegion setValue:regionList[i] forKey:@"name"];
                    [newRegion setValue:stationIDS forKey:@"stationID"];
                    NSError* error = nil;
                    if (![managedObjectContext save:&error]) {
                        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
                    }
                }
            //region data found
            }else{
                for (NSManagedObject* region in regionData) {
                    //update old regions
                    if ([regionList containsObject:[region valueForKey:@"name"]]) {
                        NSData* stationIDS = [NSKeyedArchiver archivedDataWithRootObject:regions[@"stationList"][[region valueForKey:@"name"]]];
                        [region setValue:stationIDS forKey:@"stationID"];
                        [regionList removeObject:[region valueForKey:@"name"]];
                    //delete removed regions
                    } else if(![regionList containsObject:[region valueForKey:@"name"]]){
                        [managedObjectContext deleteObject:region];
                    }
                }
                //add new regions
                for (NSString* regionID in regionList) {
                    NSData* stationIDS = [NSKeyedArchiver archivedDataWithRootObject:regions[@"stationList"][regionID]];
                    NSManagedObject* newRegion = [NSEntityDescription insertNewObjectForEntityForName:@"Region" inManagedObjectContext:managedObjectContext];
                    [newRegion setValue:regionID forKey:@"name"];
                    [newRegion setValue:stationIDS forKey:@"stationID"];
                }
                
                NSError* error = nil;
                if (![managedObjectContext save:&error]) {
                    NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
                }
            }

            [[NSNotificationCenter defaultCenter]postNotificationName:@"RegionsDictionary" object:regions];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            currentSelection = nil;
            selectedCounties = nil;
            [_indicator stopAnimating];
            [self.navigationController pushViewController:regionsView animated:YES];
        });
        
    });
    
    
}

-(void)goToTrendChart{
    TrendChartViewController* trendChartView = [[TrendChartViewController alloc]init];
    trendChartView.website_id = [currentSelection valueForKey:@"websiteID"];
    [self.navigationController presentViewController:trendChartView animated:YES completion:nil];
}

-(void)goToLegend{
    LegendViewController* legendView = [[LegendViewController alloc]init];
    
    [self.navigationController pushViewController:legendView animated:YES];
}

-(void)goToAdvisory{
    AdvisoryViewController* advisoryView = [AdvisoryViewController new];
    
    [self.navigationController pushViewController:advisoryView animated:YES];
}

-(void)goToDisclaimer{
    DisclaimerViewController* disclaimerView = [DisclaimerViewController new];
    
    [self.navigationController pushViewController:disclaimerView animated:YES];
}

-(void)goToAbout{
    AboutViewController* aboutView = [AboutViewController new];
    
    [self.navigationController pushViewController:aboutView animated:YES];
}

#pragma mark slideout menu functions

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return countyData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* identifier = @"cell";
    
    NSManagedObject* temp = countyData[indexPath.row];
    
    //set text color for polutants
    NSArray* pmColor;
    if ([[temp valueForKey:@"pm25Color"] isEqual:@"000000"]) {
        pmColor = @[@255.0,@255.0,@255.0];
    }else{
        pmColor = [[temp valueForKey:@"pm25Color"] componentsSeparatedByString:@","];
    }
    NSArray* ozoneColor;
    if ([[temp valueForKey:@"ozoneColor"] isEqual:@"000000"]) {
        ozoneColor = @[@255.0,@255.0,@255.0];
    }else{
        ozoneColor = [[temp valueForKey:@"ozoneColor"] componentsSeparatedByString:@","];
    }
    
    
    MenuDrawerTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[MenuDrawerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell setCountyName:[temp valueForKey:@"label"] withCountyCityName:[temp valueForKey:@"name"]];
    
    [cell setPM25DataValue:[temp valueForKey:@"pm25Value"] withColor:[UIColor colorWithRed:[pmColor[0] floatValue]/255.0 green:[pmColor[1] floatValue]/255.0 blue:[pmColor[2] floatValue]/255.0 alpha:1.0]];
    
    [cell setOzoneDataValue:[temp valueForKey:@"ozoneValue"] withColor:[UIColor colorWithRed:[ozoneColor[0] floatValue]/255.0 green:[ozoneColor[1] floatValue]/255.0 blue:[ozoneColor[2] floatValue]/255.0 alpha:1.0]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    currentSelection = countyData[indexPath.row];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:indexPath.row forKey:@"lastSelectedSite"];
    [defaults synchronize];
    
    [self redrawData];
    [self showFavoritesMenu:nil];
}

#pragma mark collect and display site data

-(void)refreshDATA{
    networkStatus = [networkReachability currentReachabilityStatus];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    [_indicator startAnimating];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        if (networkStatus == NotReachable) {
            
        }else{
            NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
            
            
            for (NSManagedObject* station in countyData) {
                networkStatus = [networkReachability currentReachabilityStatus];
                if(networkStatus == NotReachable)
                    break;
                NSURL* url = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://air.utah.gov/phoneApp.php?p=station&id=%@",[station valueForKey:@"stationID"]]];
                NSURLRequest* stationRequest = [NSURLRequest requestWithURL:url];
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;


//                __block NSData* tempData = nil;
//                [[NSURLSession sharedSession] dataTaskWithRequest:stationRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//                    if (error) {
//                        NSLog(@"Error: %@ %@", error, [error userInfo]);
//                    }
//                    tempData = data;
//                }];
                NSData* tempData = [NSURLConnection sendSynchronousRequest:stationRequest returningResponse:nil error:nil];
                
                NSDictionary* stationValue = [NSJSONSerialization JSONObjectWithData:tempData options:NSJSONReadingMutableContainers error:nil];
                
                NSData* backgroundIMG;
                NSData* iconIMG;
                
                if ([[station valueForKey:@"backgroundVersion"] isEqualToString:stationValue[@"station"][0][@"backgroundVersion"]]) {
                    
                } else {
                    NSURL* temp = [NSURL URLWithString:stationValue[@"station"][0][@"background"]];
                    backgroundIMG = [NSData dataWithContentsOfURL:temp];
                    [station setValue:stationValue[@"station"][0][@"background"] forKey:@"backgroundURL"];
                    [station setValue:stationValue[@"station"][0][@"backgroundVersion"] forKey:@"backgroundVersion"];
                    [station setValue:backgroundIMG forKey:@"backgroundImage"];
                }
                
                if ([[station valueForKey:@"iconVersion"] isEqualToString:stationValue[@"station"][0][@"iconVersion"]]) {
                    
                } else {
                    NSURL* temp = [NSURL URLWithString:stationValue[@"station"][0][@"icon"]];
                    iconIMG = [NSData dataWithContentsOfURL:temp];
                    [station setValue:stationValue[@"station"][0][@"icon"] forKey:@"iconURL"];
                    [station setValue:stationValue[@"station"][0][@"iconVersion"] forKey:@"iconVersion"];
                    [station setValue:iconIMG forKey:@"iconImage"];
                    
                }
                
                //station data
                [station setValue:stationValue[@"station"][0][@"label"] forKey:@"label"];
                [station setValue:stationValue[@"station"][0][@"latitude"] forKey:@"latitude"];
                [station setValue:stationValue[@"station"][0][@"longitude"] forKey:@"longitude"];
                [station setValue:stationValue[@"station"][0][@"name"] forKey:@"name"];
                [station setValue:stationValue[@"station"][0][@"region"] forKey:@"region"];
                //[station setValue:stationValue[@"station"][0][@"id"] forKey:@"stationID"];
                [station setValue:stationValue[@"station"][0][@"websiteId"] forKey:@"websiteID"];
                [station setValue:stationValue[@"station"][0][@"zip"] forKey:@"zip"];
                
                //health and forecast data
                NSData* storedForecasts = [NSKeyedArchiver archivedDataWithRootObject:stationValue[@"forecast"]];
                [station setValue:storedForecasts forKey:@"forecast"];
                //placeholder for health data
                NSArray* healthForecast = [[NSArray alloc]init];
                NSData* storedHealth = [NSKeyedArchiver archivedDataWithRootObject:healthForecast];
                [station setValue:storedHealth forKey:@"health"];
                
                //pm25 data
                [station setValue:stationValue[@"parameters"][@"pm25"][@"arrow"] forKey:@"pm25Arrow"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"color"] forKey:@"pm25Color"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"colorHex"] forKey:@"pm25HexColor"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"range"] forKey:@"pm25Range"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"unit"] forKey:@"pm25Unit"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"value"] forKey:@"pm25Value"];
                
                //ozone data
                [station setValue:stationValue[@"parameters"][@"ozone"][@"arrow"] forKey:@"ozoneArrow"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"color"] forKey:@"ozoneColor"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"colorHex"] forKey:@"ozoneHexColor"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"range"] forKey:@"ozoneRange"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"unit"] forKey:@"ozoneUnit"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"value"] forKey:@"ozoneValue"];
                
                
                //general parameter data
                if ([stationValue[@"parameters"][@"temperature"] isKindOfClass:[NSNumber class]]) {
                    [station setValue:[stationValue[@"parameters"][@"temperature"] stringValue] forKey:@"temperature"];
                }else{
                    [station setValue:stationValue[@"parameters"][@"temperature"] forKey:@"temperature"];
                }
                [station setValue:stationValue[@"parameters"][@"wind_dir"] forKey:@"windDirection"];
                if ([stationValue[@"parameters"][@"wind_speed"] isKindOfClass:[NSNumber class]]) {
                    [station setValue:[stationValue[@"parameters"][@"wind_speed"] stringValue] forKey:@"windSpeed"];
                }else{
                    [station setValue:stationValue[@"parameters"][@"wind_speed"] forKey:@"windSpeed"];
                }
                
                //other data
                [station setValue:stationValue[@"seasonalParameter"] forKey:@"seasonalParameter"];
                [station setValue:stationValue[@"version"] forKey:@"version"];
                [station setValue:[NSDate date] forKey:@"dateUpdated"];
                
                
                NSError* error = nil;
                if (![managedObjectContext save:&error]) {
                    NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
                }
            }
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
            //NSLog(@"Refreshed");
            [_indicator stopAnimating];
            [self redrawData];
            [menuDrawer reloadData];
        });
    });
    
    
}

-(void)redrawData{
    NSData* backgroundData = [currentSelection valueForKey:@"backgroundImage"];

    [background setImage:[UIImage imageWithData:backgroundData]];
    
    //display site name
    NSMutableString* county = [NSMutableString stringWithString:[currentSelection valueForKey:@"label"]];
    [county appendString:@" - "];
    [county appendString:[currentSelection valueForKey:@"name"]];
    
    [location setText:county];
    
    //display polutant, range, and severity
    if ([polutant isEqualToString:@"ozone"]) {
        NSString* range = [currentSelection valueForKey:@"ozoneRange"];
        NSArray* rangeSpliter = [range componentsSeparatedByString:@" "];
        
        [pmCount setText:[currentSelection valueForKey:@"ozoneValue"]];
        NSArray* displayColor;
        if ([[currentSelection valueForKey:@"ozoneColor"] isEqual:@"000000"]) {
            displayColor = @[@255.0,@255.0,@255.0];
        }else{
            displayColor = [[currentSelection valueForKey:@"ozoneColor"] componentsSeparatedByString:@","];
        }
        
        if (rangeSpliter.count > 1) {
            NSMutableString* rangeValue = [[NSString stringWithFormat:@"%@ %@ %@",rangeSpliter[rangeSpliter.count - 3],rangeSpliter[rangeSpliter.count - 2],rangeSpliter[rangeSpliter.count-1]]mutableCopy];
            [rangeValue appendString:[currentSelection valueForKey:@"ozoneUnit"]];
            
            NSMutableString* conditionValue = [rangeSpliter[0]mutableCopy];

            for (int i = 1; i < rangeSpliter.count - 3; i++) {
                [conditionValue appendString:[NSString stringWithFormat:@" %@",rangeSpliter[i]]];
            }
            [pollutantRange setText:rangeValue];
            [pollutionUnit setText:conditionValue];
        }else{
            [pollutantRange setText:@""];
            [pollutionUnit setText:@""];
        }
        
        
        [pmCount setTextColor:[UIColor colorWithRed:[displayColor[0] floatValue]/255.0 green:[displayColor[1] floatValue]/255.0 blue:[displayColor[2] floatValue]/255.0 alpha:1.0]];
        
        
        [pollutionUnit setTextColor:[UIColor colorWithRed:[displayColor[0] floatValue]/255.0 green:[displayColor[1] floatValue]/255.0 blue:[displayColor[2] floatValue]/255.0 alpha:1.0]];
        NSMutableString* arrow = [NSMutableString stringWithString:@"trendarrow_"];
        [arrow appendString:[currentSelection valueForKey:@"ozoneArrow"]];
        
        [trendArrow setImage:[UIImage imageNamed:arrow]];
    }else{
        NSString* range = [currentSelection valueForKey:@"pm25Range"];
        NSArray* rangeSpliter = [range componentsSeparatedByString:@" "];
        
        [pmCount setText:[currentSelection valueForKey:@"pm25Value"]];
        NSArray* displayColor;
        if ([[currentSelection valueForKey:@"pm25Color"] isEqual:@"000000"]) {
            displayColor = @[@255.0,@255.0,@255.0];
        }else{
            displayColor = [[currentSelection valueForKey:@"pm25Color"] componentsSeparatedByString:@","];
        }
        if (rangeSpliter.count > 1) {
            NSMutableString* rangeValue = [[NSString stringWithFormat:@"%@ %@ %@",rangeSpliter[rangeSpliter.count - 3],rangeSpliter[rangeSpliter.count - 2],rangeSpliter[rangeSpliter.count-1]]mutableCopy];
            [rangeValue appendString:@" \u00b5g/m\u00b3"];
            NSMutableString* conditionValue = [rangeSpliter[0]mutableCopy];

            for (int i = 1; i < rangeSpliter.count - 3; i++) {
                [conditionValue appendString:[NSString stringWithFormat:@" %@",rangeSpliter[i]]];
            }
            [pollutantRange setText:rangeValue];
            [pollutionUnit setText:conditionValue];
        }else{
            [pollutantRange setText:@""];
            [pollutionUnit setText:@""];
        }
        
        
        [pmCount setTextColor:[UIColor colorWithRed:[displayColor[0] floatValue]/255.0 green:[displayColor[1] floatValue]/255.0 blue:[displayColor[2] floatValue]/255.0 alpha:1.0]];
        

        [pollutionUnit setTextColor:[UIColor colorWithRed:[displayColor[0] floatValue]/255.0 green:[displayColor[1] floatValue]/255.0 blue:[displayColor[2] floatValue]/255.0 alpha:1.0]];
        NSMutableString* arrow = [NSMutableString stringWithString:@"trendarrow_"];
        [arrow appendString:[currentSelection valueForKey:@"pm25Arrow"]];
        
        [trendArrow setImage:[UIImage imageNamed:arrow]];
    }
    
    //display temperature
    NSString* temp = [NSString stringWithFormat:@"%@",[currentSelection valueForKey:@"temperature"]];
    if (temp.length > 5) {
        [temperatureValue setText:[temp substringToIndex:4]];
    }else{
        [temperatureValue setText:temp];
    }
    
    //display wind speed and direction
    NSMutableString* wind = [NSMutableString stringWithString:[currentSelection valueForKey:@"windDirection"]];
    [wind appendString:@" "];
    NSString* windspeed = [currentSelection valueForKey:@"windSpeed"];
    if (windspeed.length > 5) {
        [wind appendString:[NSString stringWithFormat:@"%@",[windspeed substringToIndex:4]]];
    }else{
        [wind appendString:[NSString stringWithFormat:@"%@",[currentSelection valueForKey:@"windSpeed"]]];
    }
    
    [wind appendString:@"mph"];
    [windValue setText:wind];
    
    //display health and burn forecast
    NSArray* forecast = [NSKeyedUnarchiver unarchiveObjectWithData:[currentSelection valueForKey:@"forecast"]];
    if (forecast.count > 0) {
        //today
        NSString* severity = [[forecast[0][@"severity"] stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
        NSString* todayForecast = [NSString stringWithFormat:@"%@%@",[[forecast[0][@"action"] substringToIndex:1] capitalizedString],[forecast[0][@"action"] substringFromIndex:1]];
        [todayBurnlbl setText:todayForecast];
        NSString* today = [NSString stringWithFormat:@"%@%@",[[forecast[0][@"severity"] substringToIndex:1] capitalizedString],[forecast[0][@"severity"] substringFromIndex:1]];
        [todayHealthLbl setText:today];
        [todayHealthIMGView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"health%@.png",severity]]];
        [todayBurnImgView setImage:[UIImage imageNamed:forecast[0][@"action"]]];
        [todayForcastLbl setText:forecast[0][@"dayOfWeek"]];
        
        //tomorrow
        severity = [[forecast[1][@"severity"] stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
        NSString* tomorrowForecast =[NSString stringWithFormat:@"%@%@",[[forecast[1][@"action"] substringToIndex:1] capitalizedString],[forecast[1][@"action"] substringFromIndex:1]];
        [tomorrowBurnlbl setText:tomorrowForecast];
        
        NSString* tomorrow = [NSString stringWithFormat:@"%@%@",[[forecast[1][@"severity"] substringToIndex:1] capitalizedString],[forecast[1][@"severity"] substringFromIndex:1]];
        [tomorrowHealthLbl setText:tomorrow];
        [tomorrowHealthIMGView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"health%@.png",severity]]];
        [tomorrowBurnImgView setImage:[UIImage imageNamed:forecast[1][@"action"]]];
        [tomorrowForcastLbl setText:forecast[1][@"dayOfWeek"]];
        
        //the day after tomorrow
        severity = [[forecast[2][@"severity"] stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
        NSString* nextDayForecast =[NSString stringWithFormat:@"%@%@",[[forecast[2][@"action"] substringToIndex:1] capitalizedString],[forecast[2][@"action"] substringFromIndex:1]];
        [nextdayBurnlbl setText:nextDayForecast];
        NSString* nextDay =[NSString stringWithFormat:@"%@%@",[[forecast[2][@"severity"] substringToIndex:1] capitalizedString],[forecast[2][@"severity"] substringFromIndex:1]];
        [nextdayHealthLbl setText:nextDay];
        [nextdayHealthIMGView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"health%@.png",severity]]];
        [nextdayBurnImgView setImage:[UIImage imageNamed:forecast[2][@"action"]]];
        [nextdayForcastLbl setText:forecast[2][@"dayOfWeek"]];
    }
    else{
        //if no forecast data available display this message
        [todayBurnlbl setText:@""];
        [todayHealthLbl setText:@""];
        [todayBurnImgView setImage:nil];
        [todayHealthIMGView setImage:nil];
        [todayForcastLbl setText:@""];
        [tomorrowBurnlbl setText:@""];
        [tomorrowHealthLbl setText:@"Forecast Data Unavailable"];
        [tomorrowBurnImgView setImage:nil];
        [tomorrowHealthIMGView setImage:nil];
        [tomorrowForcastLbl setText:@""];
        [nextdayBurnlbl setText:@""];
        [nextdayHealthLbl setText:@""];
        [nextdayBurnImgView setImage:nil];
        [nextdayHealthIMGView setImage:nil];
        [nextdayForcastLbl setText:@""];
    }
    
    //display date station information was last updated
    NSDateFormatter* formatNow = [[NSDateFormatter alloc]init];
    [formatNow setDateFormat:@"MM/dd/yy HH:mm"];
    [dateTime setText:[formatNow  stringFromDate:[currentSelection valueForKey:@"dateUpdated"]]];
}

#pragma mark orientation controll

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}


@end
