//
//  CountyViewController.h
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountyTableViewCell.h"

@interface CountyViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray* counties;
    UITableView* countyTable;
    NSMutableArray* selectedCounties;
    NSMutableArray* countyData;
    NSMutableArray* selectedCells;
}

@end
