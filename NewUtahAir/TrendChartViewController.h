//
//  TrendChartViewController.h
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrendChartViewController : UIViewController<UIWebViewDelegate>

{
    UIWebView* trendChart;
}

@property (nonatomic,strong) NSString* website_id;

@end
