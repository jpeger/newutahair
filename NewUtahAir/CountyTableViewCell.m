//
//  CountyTableViewCell.m
//  NewUtahAir
//
//  Created by Developement on 2/6/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "CountyTableViewCell.h"

@implementation CountyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self setBackgroundColor:[UIColor blackColor]];
    
    countyImage = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, self.frame.size.width/4 - 10, self.frame.size.height - 10)];
    //[countyImage setBackgroundColor:[UIColor whiteColor]];
    [countyImage setContentMode:UIViewContentModeScaleAspectFit];
    
    
    countyName = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/4, 2, self.frame.size.width - 10 - self.frame.size.width/4, self.frame.size.height/3)];
    [countyName setText:@"Place Holder"];
    
    [countyName setTextColor:[UIColor colorWithRed:0.608 green:0.451 blue:0.663 alpha:1.0]];
    [countyName setTextAlignment:NSTextAlignmentCenter];
    [countyName setFont:[UIFont systemFontOfSize:12]];
    
    
    UILabel* pm25 = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/4, countyName.frame.size.height+countyName.frame.origin.y+2, self.frame.size.width/4 - 10, self.frame.size.height/3 - 4)];
    [pm25 setText:@"PM 2.5:"];
    [pm25 setTextColor:[UIColor whiteColor]];
    [pm25 setTextAlignment:NSTextAlignmentRight];
    [pm25 setFont:[UIFont systemFontOfSize:12]];
    
    
    UILabel* ozone = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/4, pm25.frame.size.height+pm25.frame.origin.y+2, self.frame.size.width/4 - 10, self.frame.size.height/3 - 4)];
    [ozone setText:@"Ozone:"];
    [ozone setTextColor:[UIColor whiteColor]];
    [ozone setTextAlignment:NSTextAlignmentRight];
    [ozone setFont:[UIFont systemFontOfSize:12]];
    
    
    pm25DataValue = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/2, countyName.frame.size.height+countyName.frame.origin.y+2, self.frame.size.width/4 - 10, self.frame.size.height/3 - 4)];
    [pm25DataValue setTextColor:[UIColor whiteColor]];
    [pm25DataValue setTextAlignment:NSTextAlignmentLeft];
    [pm25DataValue setFont:[UIFont systemFontOfSize:12]];
    
    
    ozoneDataValue = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/2, pm25DataValue.frame.size.height+pm25DataValue.frame.origin.y+2, self.frame.size.width/4 - 10, self.frame.size.height/3 - 4)];
    [ozoneDataValue setTextColor:[UIColor whiteColor]];
    [ozoneDataValue setTextAlignment:NSTextAlignmentLeft];
    [ozoneDataValue setFont:[UIFont systemFontOfSize:12]];
    
    [self addSubview:countyImage];
    [self addSubview:countyName];
    [self addSubview:pm25];
    [self addSubview:ozone];
    [self addSubview:pm25DataValue];
    [self addSubview:ozoneDataValue];
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCountyImage:(UIImage *)logo{
    [countyImage setImage:logo];
}

-(void)setCountyName:(NSString *)countyNameString withCountyCityName:(NSString *)countyCityName{
    NSMutableString* county = [NSMutableString stringWithString:countyNameString];
    [county appendString:@" - "];
    [county appendString:countyCityName];
    
    [countyName setText:county];
}

-(void)setPM25DataValue:(NSString *)pmValue withColor:(UIColor*) colorValue{
    [pm25DataValue setText:pmValue];
    [pm25DataValue setTextColor:colorValue];
}

-(void)setOzoneDataValue:(NSString *)ozoneValue withColor:(UIColor*) colorValue{
    [ozoneDataValue setText:ozoneValue];
    [ozoneDataValue setTextColor:colorValue];
}

@end
