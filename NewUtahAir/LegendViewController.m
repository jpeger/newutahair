//
//  LegendViewController.m
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "LegendViewController.h"

@interface LegendViewController ()

@end

@implementation LegendViewController

#pragma mark view functions

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Forecast Legend";
    
    //add forcast legend view
    legend = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-65)];
    [legend setDelegate:self];
    [legend scalesPageToFit];
    [self.view addSubview:legend];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //load legend page
    NSURL* legendURl = [NSURL URLWithString:@"http://air.utah.gov/phoneApp.php?p=educ"];
    NSURLRequest* legendRequest = [NSURLRequest requestWithURL:legendURl];
    
    NSOperationQueue* queue = [[NSOperationQueue alloc]init];
    [NSURLConnection sendAsynchronousRequest:legendRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if([data length] > 0 && connectionError == nil){
            [legend loadRequest:legendRequest];
        }else if (connectionError != nil){
            NSLog(@"Error: %@", connectionError);
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //NSLog(@"will stop loading");
    [legend stopLoading];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark web view functions

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    //NSLog(@"Did start load");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    //NSLog(@"Did end load");
}

#pragma mark orientation controll

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}


@end
