//
//  RegionsViewController.m
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "RegionsViewController.h"

@interface RegionsViewController ()

@end

@implementation RegionsViewController

#pragma mark util functions

-(NSManagedObjectContext*)managedObjectContext{
    NSManagedObjectContext* contex = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        contex = [delegate managedObjectContext];
    }
    return contex;
}

#pragma mark view functions

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //check network status
    networkReachability = [Reachability reachabilityForInternetConnection];
    networkStatus = [networkReachability currentReachabilityStatus];
    
    UIBarButtonItem* saveFavorites = [[UIBarButtonItem alloc]initWithTitle:@"Continue" style:UIBarButtonItemStylePlain target:self action:@selector(goToCounty)];
    self.navigationItem.rightBarButtonItem = saveFavorites;
    self.navigationItem.title = @"Regions";
    
    UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc]init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    regionsCollection = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64) collectionViewLayout:layout];
    regionsCollection.allowsMultipleSelection = YES;
    regionsCollection.dataSource = self;
    regionsCollection.delegate = self;
    [regionsCollection registerClass:[RegionsCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [regionsCollection setBackgroundColor:[UIColor darkGrayColor]];
    
    [self.view addSubview:regionsCollection];
    siteList = [[NSMutableArray alloc]init];
    siteListDictionary =[[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recievedRegions:) name:@"RegionsDictionary" object:nil];
    
    NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Region"];
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    regionData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    self.navigationItem.hidesBackButton = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Region"];
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    regionData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [regionsCollection reloadData];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    
    if (networkStatus == NotReachable) {
        
    } else {
        [self storeCountyData];
    }
    
    [regionsCollection reloadData];
    //NSLog(@"At will Appear: %@",regionData);
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [regionsCollection reloadData];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //NSLog(@"%@",siteList);
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SiteListDictionary" object:siteList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark collection view functions

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return regionData.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    RegionsCollectionViewCell* cell = [regionsCollection dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    [cell setRegionName:[[regionData objectAtIndex:indexPath.row] valueForKey:@"name"]];
    [cell setBackgroundImage:[UIImage imageNamed:[[[regionData objectAtIndex:indexPath.row] valueForKey:@"name"]lowercaseString]]];
    NSMutableString* region = [NSMutableString stringWithString:[[[regionData objectAtIndex:indexPath.row] valueForKey:@"name"]lowercaseString]];
    [region appendString:@"_county"];
    [cell setRegionImage:[UIImage imageNamed:region]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[collectionView cellForItemAtIndexPath:indexPath] setBackgroundColor:[UIColor greenColor]];
    for (NSString* site in [NSKeyedUnarchiver unarchiveObjectWithData:[[regionData objectAtIndex:indexPath.row] valueForKey:@"stationID"]]) {
        [siteList addObject:site];
        //NSLog(@"%@",siteList);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[collectionView cellForItemAtIndexPath:indexPath] setBackgroundColor:[UIColor clearColor]];
    for (NSString* site in [NSKeyedUnarchiver unarchiveObjectWithData:[[regionData objectAtIndex:indexPath.row] valueForKey:@"stationID"]]) {
        [siteList removeObject:site];
        //NSLog(@"%@",siteList);
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGSize dim = CGSizeMake(self.view.frame.size.width - 20, 100);
    
    return dim;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

#pragma mark navigation functions

//Responds to the regions json being loaded
//Takes the region names and loads them for display
-(void)recievedRegions:(NSNotification*) notification{
    NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Region"];
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    regionData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [regionsCollection reloadData];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    
    if (networkStatus == NotReachable) {
        
    } else {
        [self storeCountyData];
    }
    
}

-(void)goToCounty{
    CountyViewController* countyView = [[CountyViewController alloc]init];
    networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
    } else {
        for (NSString* siteID in siteList) {
            NSURL* url = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://air.utah.gov/phoneApp.php?p=station&id=%@",siteID]];
            NSURLRequest* stationRequest = [NSURLRequest requestWithURL:url];
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
            NSOperationQueue* queue = [[NSOperationQueue alloc]init];
            [NSURLConnection sendAsynchronousRequest:stationRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                
                NSDictionary* stationList = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                [siteListDictionary addObject:stationList];
                
            }];
        }
    }
    
    
    if (siteList.count < 1) {
        
    } else {
        [self.navigationController pushViewController:countyView animated:YES];
    }
    
    
}

#pragma mark store county data

-(void)storeCountyData{
    NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Station"];
    countyData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSMutableArray* stationList = [[NSMutableArray alloc] init];
    for (NSManagedObject* region in regionData) {
        for (NSString* site in [NSKeyedUnarchiver unarchiveObjectWithData:[region  valueForKey:@"stationID"]]) {
            [stationList addObject:site];
        }
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (NSManagedObject* station in countyData) {
            //update station if it exsists
            if ([stationList containsObject:[station valueForKey:@"stationID"]]) {
                //update station values
                NSURL* url = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://air.utah.gov/phoneApp.php?p=station&id=%@",[station valueForKey:@"stationID"]]];
                NSURLRequest* stationRequest = [NSURLRequest requestWithURL:url];
                
                
                //NSOperationQueue* queue = [[NSOperationQueue alloc]init];
                NSData* data = [NSURLConnection sendSynchronousRequest:stationRequest returningResponse:nil error:nil];
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
                NSDictionary* stationValue = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                //get background and icon image if they have changed
                NSData* backgroundIMG;
                NSData* iconIMG;
                
                if ([[station valueForKey:@"backgroundVersion"] isEqualToString:stationValue[@"station"][0][@"backgroundVersion"]]) {
                    
                } else {
                    NSURL* temp = [NSURL URLWithString:stationValue[@"station"][0][@"background"]];
                    backgroundIMG = [NSData dataWithContentsOfURL:temp];
                    [station setValue:stationValue[@"station"][0][@"background"] forKey:@"backgroundURL"];
                    [station setValue:stationValue[@"station"][0][@"backgroundVersion"] forKey:@"backgroundVersion"];
                    [station setValue:backgroundIMG forKey:@"backgroundImage"];
                }
                
                if ([[station valueForKey:@"iconVersion"] isEqualToString:stationValue[@"station"][0][@"iconVersion"]]) {
                    
                } else {
                    NSURL* temp = [NSURL URLWithString:stationValue[@"station"][0][@"icon"]];
                    iconIMG = [NSData dataWithContentsOfURL:temp];
                    [station setValue:stationValue[@"station"][0][@"icon"] forKey:@"iconURL"];
                    [station setValue:stationValue[@"station"][0][@"iconVersion"] forKey:@"iconVersion"];
                    [station setValue:iconIMG forKey:@"iconImage"];
                    
                }
                
                //station data
                [station setValue:stationValue[@"station"][0][@"label"] forKey:@"label"];
                [station setValue:stationValue[@"station"][0][@"latitude"] forKey:@"latitude"];
                [station setValue:stationValue[@"station"][0][@"longitude"] forKey:@"longitude"];
                [station setValue:stationValue[@"station"][0][@"name"] forKey:@"name"];
                [station setValue:stationValue[@"station"][0][@"region"] forKey:@"region"];
                [station setValue:stationValue[@"station"][0][@"websiteId"] forKey:@"websiteID"];
                [station setValue:stationValue[@"station"][0][@"zip"] forKey:@"zip"];
                
                //health and forecast data
                NSData* storedForecasts = [NSKeyedArchiver archivedDataWithRootObject:stationValue[@"forecast"]];
                [station setValue:storedForecasts forKey:@"forecast"];
                //placeholder for health data
                NSArray* healthForecast = [[NSArray alloc]init];
                NSData* storedHealth = [NSKeyedArchiver archivedDataWithRootObject:healthForecast];
                [station setValue:storedHealth forKey:@"health"];
                
                //pm25 data
                [station setValue:stationValue[@"parameters"][@"pm25"][@"arrow"] forKey:@"pm25Arrow"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"color"] forKey:@"pm25Color"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"colorHex"] forKey:@"pm25HexColor"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"range"] forKey:@"pm25Range"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"unit"] forKey:@"pm25Unit"];
                [station setValue:stationValue[@"parameters"][@"pm25"][@"value"] forKey:@"pm25Value"];
                
                //ozone data
                [station setValue:stationValue[@"parameters"][@"ozone"][@"arrow"] forKey:@"ozoneArrow"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"color"] forKey:@"ozoneColor"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"colorHex"] forKey:@"ozoneHexColor"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"range"] forKey:@"ozoneRange"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"unit"] forKey:@"ozoneUnit"];
                [station setValue:stationValue[@"parameters"][@"ozone"][@"value"] forKey:@"ozoneValue"];
                
                
                //general parameter data
                if ([stationValue[@"parameters"][@"temperature"] isKindOfClass:[NSNumber class]]) {
                    [station setValue:[stationValue[@"parameters"][@"temperature"] stringValue] forKey:@"temperature"];
                }else{
                    [station setValue:stationValue[@"parameters"][@"temperature"] forKey:@"temperature"];
                }
                [station setValue:stationValue[@"parameters"][@"wind_dir"] forKey:@"windDirection"];
                if ([stationValue[@"parameters"][@"wind_speed"] isKindOfClass:[NSNumber class]]) {
                    [station setValue:[stationValue[@"parameters"][@"wind_speed"] stringValue] forKey:@"windSpeed"];
                }else{
                    [station setValue:stationValue[@"parameters"][@"wind_speed"] forKey:@"windSpeed"];
                }
                
                //other data
                [station setValue:stationValue[@"seasonalParameter"] forKey:@"seasonalParameter"];
                [station setValue:stationValue[@"version"] forKey:@"version"];
                [station setValue:[NSDate date] forKey:@"dateUpdated"];
                
                
                NSError* error = nil;
                if (![managedObjectContext save:&error]) {
                    NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
                }
                [stationList removeObject:[station valueForKey:@"stationID"]];
            }
        }
        for (NSString* stationID in stationList) {
            //add new station
            NSURL* url = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://air.utah.gov/phoneApp.php?p=station&id=%@",stationID]];
            NSURLRequest* stationRequest = [NSURLRequest requestWithURL:url];
            
            NSData* data = [NSURLConnection sendSynchronousRequest:stationRequest returningResponse:nil error:nil];
            
            NSDictionary* stationValue = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            NSManagedObject* station = [NSEntityDescription insertNewObjectForEntityForName:@"Station" inManagedObjectContext:managedObjectContext];
            
            //station data
            [station setValue:stationValue[@"station"][0][@"label"] forKey:@"label"];
            [station setValue:stationValue[@"station"][0][@"latitude"] forKey:@"latitude"];
            [station setValue:stationValue[@"station"][0][@"longitude"] forKey:@"longitude"];
            [station setValue:stationValue[@"station"][0][@"name"] forKey:@"name"];
            [station setValue:stationValue[@"station"][0][@"region"] forKey:@"region"];
            [station setValue:stationValue[@"station"][0][@"id"] forKey:@"stationID"];
            [station setValue:stationValue[@"station"][0][@"websiteId"] forKey:@"websiteID"];
            [station setValue:stationValue[@"station"][0][@"zip"] forKey:@"zip"];
            
            //background data
            NSURL* backgroundURL = [NSURL URLWithString:stationValue[@"station"][0][@"background"]];
            NSData* backgroundIMG = [NSData dataWithContentsOfURL:backgroundURL];
            [station setValue:stationValue[@"station"][0][@"background"] forKey:@"backgroundURL"];
            [station setValue:stationValue[@"station"][0][@"backgroundVersion"] forKey:@"backgroundVersion"];
            [station setValue:backgroundIMG forKey:@"backgroundImage"];
            
            //icon data
            NSURL* iconURL = [NSURL URLWithString:stationValue[@"station"][0][@"icon"]];
            NSData* iconIMG = [NSData dataWithContentsOfURL:iconURL];
            [station setValue:stationValue[@"station"][0][@"icon"] forKey:@"iconURL"];
            [station setValue:stationValue[@"station"][0][@"iconVersion"] forKey:@"iconVersion"];
            [station setValue:iconIMG forKey:@"iconImage"];
            
            //health and forecast data
            NSData* storedForecasts = [NSKeyedArchiver archivedDataWithRootObject:stationValue[@"forecast"]];
            [station setValue:storedForecasts forKey:@"forecast"];
            //placeholder for health data
            NSArray* healthForecast = [[NSArray alloc]init];
            NSData* storedHealth = [NSKeyedArchiver archivedDataWithRootObject:healthForecast];
            [station setValue:storedHealth forKey:@"health"];
            
            //pm25 data
            [station setValue:stationValue[@"parameters"][@"pm25"][@"arrow"] forKey:@"pm25Arrow"];
            [station setValue:stationValue[@"parameters"][@"pm25"][@"color"] forKey:@"pm25Color"];
            [station setValue:stationValue[@"parameters"][@"pm25"][@"colorHex"] forKey:@"pm25HexColor"];
            [station setValue:stationValue[@"parameters"][@"pm25"][@"range"] forKey:@"pm25Range"];
            [station setValue:stationValue[@"parameters"][@"pm25"][@"unit"] forKey:@"pm25Unit"];
            [station setValue:stationValue[@"parameters"][@"pm25"][@"value"] forKey:@"pm25Value"];
            
            //ozone data
            [station setValue:stationValue[@"parameters"][@"ozone"][@"arrow"] forKey:@"ozoneArrow"];
            [station setValue:stationValue[@"parameters"][@"ozone"][@"color"] forKey:@"ozoneColor"];
            [station setValue:stationValue[@"parameters"][@"ozone"][@"colorHex"] forKey:@"ozoneHexColor"];
            [station setValue:stationValue[@"parameters"][@"ozone"][@"range"] forKey:@"ozoneRange"];
            [station setValue:stationValue[@"parameters"][@"ozone"][@"unit"] forKey:@"ozoneUnit"];
            [station setValue:stationValue[@"parameters"][@"ozone"][@"value"] forKey:@"ozoneValue"];
            
            
            //general parameter data
            if ([stationValue[@"parameters"][@"temperature"] isKindOfClass:[NSNumber class]]) {
                [station setValue:[stationValue[@"parameters"][@"temperature"] stringValue] forKey:@"temperature"];
            }else{
                [station setValue:stationValue[@"parameters"][@"temperature"] forKey:@"temperature"];
            }
            [station setValue:stationValue[@"parameters"][@"wind_dir"] forKey:@"windDirection"];
            if ([stationValue[@"parameters"][@"wind_speed"] isKindOfClass:[NSNumber class]]) {
                [station setValue:[stationValue[@"parameters"][@"wind_speed"] stringValue] forKey:@"windSpeed"];
            }else{
                [station setValue:stationValue[@"parameters"][@"wind_speed"] forKey:@"windSpeed"];
            }
            
            //other data
            [station setValue:stationValue[@"seasonalParameter"] forKey:@"seasonalParameter"];
            [station setValue:stationValue[@"version"] forKey:@"version"];
            [station setValue:[NSDate date] forKey:@"dateUpdated"];
            
            NSError* error = nil;
            if (![managedObjectContext save:&error]) {
                NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
            }
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
        });
    });
    
}

#pragma mark orientation controll

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}

@end
