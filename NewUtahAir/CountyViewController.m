//
//  CountyViewController.m
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "CountyViewController.h"


@interface CountyViewController ()

@end

@implementation CountyViewController

#pragma mark util functions

-(NSManagedObjectContext*)managedObjectContext{
    NSManagedObjectContext* contex = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        contex = [delegate managedObjectContext];
    }
    return contex;
}

#pragma mark view functions

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedCounties = [NSMutableArray new];
    selectedCells = [NSMutableArray array];
    
    countyTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
    [countyTable setDelegate:self];
    [countyTable setDataSource:self];
    [countyTable setBackgroundColor:[UIColor darkGrayColor]];
    [countyTable setRowHeight:60.0];
    countyTable.allowsMultipleSelection = YES;
    self.navigationItem.title = @"Counties";
    
    UIBarButtonItem* save = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(backToMain)];
    self.navigationItem.rightBarButtonItem = save;
    self.navigationItem.hidesBackButton = YES;
    
    [self registerForNotifications];
    
    [self.view addSubview:countyTable];

}

-(void)registerForNotifications{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recievedCounties:) name:@"SiteListDictionary" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SelectedCounties" object:selectedCounties];
}

#pragma mark table view functions

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return countyData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* identifier = @"cell";
    NSManagedObject* station = countyData[indexPath.row];
    NSArray* pmColor;
    if ([[station valueForKey:@"pm25Color"] isEqual:@"000000"]) {
        pmColor = @[@255.0,@255.0,@255.0];
    }else{
        pmColor = [[station valueForKey:@"pm25Color"] componentsSeparatedByString:@","];
    }
    
    NSArray* ozoneColor;
    if ([[station valueForKey:@"ozoneColor"] isEqual:@"000000"]) {
        ozoneColor = @[@255.0,@255.0,@255.0];
    }else{
        ozoneColor = [[station valueForKey:@"ozoneColor"]componentsSeparatedByString:@","];
    }

    CountyTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[CountyTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if ([selectedCells containsObject:indexPath]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }else{
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }

    [cell setCountyName:[station valueForKey:@"label"] withCountyCityName:[station valueForKey:@"name"]];
    [cell setPM25DataValue:[station valueForKey:@"pm25Value"] withColor:[UIColor colorWithRed:[pmColor[0] floatValue]/255.0 green:[pmColor[1] floatValue]/255.0 blue:[pmColor[2] floatValue]/255.0 alpha:1.0]];
    
    [cell setOzoneDataValue:[station valueForKey:@"ozoneValue"] withColor:[UIColor colorWithRed:[ozoneColor[0] floatValue]/255.0 green:[ozoneColor[1] floatValue]/255.0 blue:[ozoneColor[2] floatValue]/255.0 alpha:1.0]];

    NSData* catch = [station valueForKey:@"iconImage"];
    if ([catch isKindOfClass:[NSNull class]]) {

    }else{
        [cell setCountyImage:[UIImage imageWithData:[station valueForKey:@"iconImage"]]];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    [selectedCells addObject:indexPath];
    [selectedCounties addObject:[countyData[indexPath.row] valueForKey:@"stationID"]];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    [selectedCells removeObject:indexPath];
    [selectedCounties removeObject:[countyData[indexPath.row] valueForKey:@"stationID"]];
}

#pragma mark navigation functions

-(void)backToMain{
    if (selectedCounties.count < 1) {
        
    } else {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:selectedCounties forKey:@"selectedCounties"];
        [defaults setInteger:0 forKey:@"lastSelectedSite"];
        [defaults synchronize];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

-(void)recievedCounties:(NSNotification*) notification{
    counties = [notification object];
    NSManagedObjectContext* managedObjectContext = [self managedObjectContext];
    countyData = [[NSMutableArray alloc]init];

    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Station"];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"stationID IN %@",counties];
    [fetchRequest setPredicate:predicate];

    NSSortDescriptor* mainDesctiptor = [NSSortDescriptor sortDescriptorWithKey:@"label" ascending:YES];
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    [fetchRequest setSortDescriptors:@[mainDesctiptor,sortDescriptor]];
    
    NSError* error = nil;

    countyData = [[managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    [countyTable reloadData];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

#pragma mark orientation controll

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}


@end
