//
//  RegionsCollectionViewCell.m
//  NewUtahAir
//
//  Created by Developement on 2/27/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "RegionsCollectionViewCell.h"

@implementation RegionsCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    regionImageViewBackground = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, self.frame.size.width-10, self.frame.size.height-10)];
    regionImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, self.frame.size.height-20, self.frame.size.height-20)];
    regionName = [[UILabel alloc]initWithFrame:CGRectMake(regionImageView.frame.size.width+20, 10, self.frame.size.width-(regionImageView.frame.size.width+20)*2, self.frame.size.height-20)];
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:regionImageViewBackground];
    [self addSubview:regionImageView];
    [self addSubview:regionName];
    return self;
}

-(void)setBackgroundImage:(UIImage*)backgroundImage{
    [regionImageViewBackground setImage:backgroundImage];
    [regionImageViewBackground setContentMode:UIViewContentModeScaleToFill];
}

-(void)setRegionImage:(UIImage*)regionImage{
    [regionImageView setImage:regionImage];
    [regionImageView setContentMode:UIViewContentModeScaleAspectFit];
}

-(void)setRegionName:(NSString*)region{
    [regionName setText:region];
}

@end
