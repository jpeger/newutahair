//
//  DisclaimerViewController.m
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "DisclaimerViewController.h"

@interface DisclaimerViewController ()

@end

@implementation DisclaimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Disclaimer";
    //add backround image
    UIImage* backgrounImage = [UIImage imageNamed:@"background_iphone1.png"];
    UIImageView* background = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [background setImage:backgrounImage];
    [background setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:background];
    
    //add disclaimer statement
    UITextView* aboutStatement = [[UITextView alloc]initWithFrame:CGRectMake(25, 0, self.view.frame.size.width-45, self.view.frame.size.height/2)];
    [aboutStatement setUserInteractionEnabled:NO];
    [aboutStatement setBackgroundColor:[UIColor clearColor]];
    [aboutStatement setText:@"All information provided in the Utah Air application is for informational purposes only and does not constitute a legal contract between the NCAST and any person or entity unless otherwise specified. Information on the Utah Air application is subject to change without prior notice. Although every reasonable effort is made to present current and accurate information, NCAST makes no guarantees of any kind.\r\rThe data displayed in this app is not quality assured. for quality assured data please visit www.epa.gov/airdata/"];
    [aboutStatement sizeToFit];
    [aboutStatement setTextColor:[UIColor whiteColor]];
    [self.view addSubview:aboutStatement];
    
    //add weber state image
    UIImageView* weberLogo = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-((self.view.frame.size.width*0.5)+65), self.view.frame.size.width*0.5, self.view.frame.size.width*0.5)];
    [weberLogo setImage:[UIImage imageNamed:@"sighan"]];
    [weberLogo setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:weberLogo];
    
    //add DAQ image
    UIImageView* DAQLogo = [[UIImageView alloc]initWithFrame:CGRectMake(weberLogo.frame.origin.x+weberLogo.frame.size.width-10, self.view.frame.size.height-((self.view.frame.size.width*0.5)+65), self.view.frame.size.width*0.5, self.view.frame.size.width*0.5)];
    [DAQLogo setImage:[UIImage imageNamed:@"Q_logo2"]];
    [DAQLogo setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:DAQLogo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark orientation controll

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}



@end
