//
//  RegionsCollectionViewCell.h
//  NewUtahAir
//
//  Created by Developement on 2/27/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegionsCollectionViewCell : UICollectionViewCell
{
    UIImageView* regionImageView;
    UIImageView* regionImageViewBackground;
    UILabel* regionName;
}

-(void)setBackgroundImage:(UIImage*)backgroundImage;

-(void)setRegionImage:(UIImage*)regionImage;

-(void)setRegionName:(NSString*)region;

@end
