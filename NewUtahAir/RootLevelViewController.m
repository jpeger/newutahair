//
//  RootLevelViewController.m
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "RootLevelViewController.h"
#import "ViewController.h"

@interface RootLevelViewController ()

@end

@implementation RootLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ViewController* vc = [ViewController new];
    
    UINavigationController* navCon = [[UINavigationController alloc]initWithRootViewController:vc];
    navCon.interactivePopGestureRecognizer.enabled = NO;
    [self addChildViewController:navCon];
    [self.view addSubview:navCon.view];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

-(BOOL)shouldAutorotate{
    return NO;
}


@end
