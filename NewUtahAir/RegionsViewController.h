//
//  RegionsViewController.h
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountyViewController.h"
#import "RegionsCollectionViewCell.h"
#import "Reachability.h"

@interface RegionsViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    UICollectionView* regionsCollection;
    //NSDictionary* regions;
    NSArray* regionNames;
    NSMutableArray* siteList;
    NSMutableArray* siteListDictionary;
    NSMutableArray* regionData;
    NSMutableArray* countyData;
    Reachability* networkReachability;
    NetworkStatus networkStatus;
}

-(void)goToCounty;
@end
