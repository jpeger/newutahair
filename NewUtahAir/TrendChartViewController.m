//
//  TrendChartViewController.m
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "TrendChartViewController.h"

@interface TrendChartViewController ()

@end

@implementation TrendChartViewController

#pragma mark view functions

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Trend Chart";
    NSNumber* value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
    [[UIDevice currentDevice]setValue:value forKey:@"orientation"];
    
    
    trendChart = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-65)];
    [trendChart setDelegate:self];
    trendChart.scalesPageToFit = YES;

    UIButton* back = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [back setTitle:@"back" forState:UIControlStateNormal];
    [back setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [back setClipsToBounds:YES];
    [back setFrame:CGRectMake(10, self.view.frame.size.width-50, 60, 40)];
    [back addTarget:self action:@selector(backBtnTouched) forControlEvents:UIControlEventTouchUpInside];
    [back setContentEdgeInsets:UIEdgeInsetsMake(2, 2, 2, 2)];
    back.titleLabel.font = [UIFont systemFontOfSize:20.0];

    [self.view addSubview:trendChart];
    [self.view addSubview:back];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //load trendchart page
    NSString* trendChartLocation = [NSString stringWithFormat:@"http://air.utah.gov/phoneApp.php?&type=html&p=chart&id=%@",self.website_id];
    NSURL* legendURl = [NSURL URLWithString:trendChartLocation];
    NSURLRequest* legendRequest = [NSURLRequest requestWithURL:legendURl];
    
    NSOperationQueue* queue = [[NSOperationQueue alloc]init];
    [NSURLConnection sendAsynchronousRequest:legendRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if([data length] > 0 && connectionError == nil){
            [trendChart loadRequest:legendRequest];
        }else if (connectionError != nil){
            NSLog(@"Error: %@", connectionError);
        }
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSNumber* value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
    [[UIDevice currentDevice]setValue:value forKey:@"orientation"];
    [trendChart setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    [trendChart stopLoading];
}

#pragma navigation controll

-(void)backBtnTouched{

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark web view functions

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

#pragma mark orientation controll

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationLandscapeLeft;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeLeft;
}

-(BOOL)shouldAutorotate{
    return YES;
}


@end
