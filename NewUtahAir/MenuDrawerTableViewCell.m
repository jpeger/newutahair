//
//  MenuDrawerTableViewCell.m
//  NewUtahAir
//
//  Created by Developement on 2/6/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "MenuDrawerTableViewCell.h"

@implementation MenuDrawerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self setBackgroundColor:[UIColor clearColor]];
    
    countyName = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, self.frame.size.width-10, self.frame.size.height/2 - 5)];
    [countyName setTextColor:[UIColor whiteColor]];
    [countyName setFont:[UIFont systemFontOfSize:14]];
    
    
    UILabel* pm25 = [[UILabel alloc]initWithFrame:CGRectMake(5, self.frame.size.height/2 + 5, self.frame.size.width/4 - 25, self.frame.size.height/2 - 5)];
    [pm25 setText:@"PM 2.5:"];
    [pm25 setTextColor:[UIColor whiteColor]];
    [pm25 setFont:[UIFont systemFontOfSize:14]];
    
    
    pm25DataValue = [[UILabel alloc]initWithFrame:CGRectMake(pm25.frame.origin.x + pm25.frame.size.width, self.frame.size.height/2 + 5, self.frame.size.width/4 - 5, self.frame.size.height/2 - 5)];
    [pm25DataValue setTextColor:[UIColor whiteColor]];
    [pm25DataValue setFont:[UIFont systemFontOfSize:14]];
    
    
    UILabel* ozone = [[UILabel alloc]initWithFrame:CGRectMake(pm25DataValue.frame.origin.x + pm25DataValue.frame.size.width, self.frame.size.height/2 + 5, self.frame.size.width/4 - 25, self.frame.size.height/2 - 5)];
    [ozone setText:@"Ozone:"];
    [ozone setTextColor:[UIColor whiteColor]];
    [ozone setFont:[UIFont systemFontOfSize:14]];
    
    
    ozoneDataValue = [[UILabel alloc]initWithFrame:CGRectMake(ozone.frame.origin.x+ozone.frame.size.width, self.frame.size.height/2 + 5, self.frame.size.width/4 - 5, self.frame.size.height/2 - 5)];
    [ozoneDataValue setTextColor:[UIColor whiteColor]];
    [ozoneDataValue setFont:[UIFont systemFontOfSize:14]];
    
    [self addSubview:countyName];
    [self addSubview:pm25];
    [self addSubview:pm25DataValue];
    [self addSubview:ozone];
    [self addSubview:ozoneDataValue];
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCountyName:(NSString *)countyNameString withCountyCityName:(NSString *)countyCityName{
    NSMutableString* county = [NSMutableString stringWithString:countyNameString];
    [county appendString:@" - "];
    [county appendString:countyCityName];
    
    [countyName setText:county];
}

-(void)setPM25DataValue:(NSString *)pmValue withColor:(UIColor*) colorValue{
    [pm25DataValue setText:pmValue];
    [pm25DataValue setTextColor:colorValue];
}

-(void)setOzoneDataValue:(NSString *)ozoneValue withColor:(UIColor*) colorValue{
    [ozoneDataValue setText:ozoneValue];
    [ozoneDataValue setTextColor:colorValue];
}

@end
