//
//  main.m
//  NewUtahAir
//
//  Created by Developement on 8/31/14.
//  Copyright (c) 2014 NCAST. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
