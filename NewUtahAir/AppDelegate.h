//
//  AppDelegate.h
//  NewUtahAir
//
//  Created by Developement on 8/31/14.
//  Copyright (c) 2014 NCAST. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel* managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator* persistentStoreCoordinator;

-(void)saveContext;
-(NSURL*)applicationDocumentsDirectory;

@end
