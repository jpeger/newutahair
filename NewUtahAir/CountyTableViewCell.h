//
//  CountyTableViewCell.h
//  NewUtahAir
//
//  Created by Developement on 2/6/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountyTableViewCell : UITableViewCell
{
    UIImageView* countyImage;
    UILabel* countyName;
    UILabel* pm25DataValue;
    UILabel* ozoneDataValue;
}

-(void)setCountyImage:(UIImage*)logo;

-(void)setCountyName:(NSString*)countyNameString withCountyCityName:(NSString*)countyCityName;

-(void)setPM25DataValue:(NSString*)pmValue withColor:(UIColor*) colorValue;

-(void)setOzoneDataValue:(NSString*)ozoneValue withColor:(UIColor*) colorValue;

@end
