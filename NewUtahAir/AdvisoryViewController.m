//
//  AdvisoryViewController.m
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "AdvisoryViewController.h"

@interface AdvisoryViewController ()

@end

@implementation AdvisoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Advisory";
    
    //replace with appropriate items when advisory data available
    UILabel* temporary = [[UILabel alloc] initWithFrame:self.view.bounds];
    [temporary setText:@"Advisory data unavailable at this time"];
    
    [self.view addSubview:temporary];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark orientation controll

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}


@end
