//
//  ViewController.h
//  NewUtahAir
//
//  Created by Developement on 8/31/14.
//  Copyright (c) 2014 NCAST. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegionsViewController.h"
#import "AboutViewController.h"
#import "AdvisoryViewController.h"
#import "DisclaimerViewController.h"
#import "LegendViewController.h"
#import "TrendChartViewController.h"
#import "MenuDrawerTableViewCell.h"
#import "Reachability.h"

@interface ViewController : UIViewController <UIActionSheetDelegate, UITableViewDataSource, UITableViewDelegate>

{
    
    UILabel* pm;
    UIToolbar* menu;
    UIBarButtonItem* dropDown;
    UITableView* menuDrawer;
    NSMutableArray* selectedCounties;
    NSDictionary* currentSelection;
    UILabel* location;
    UILabel* pmCount;
    UILabel* pollutantRange;
    UILabel* pollutionUnit;
    UILabel* temperatureValue;
    UILabel* windValue;
    UITextView* todayHealthLbl;
    UITextView* tomorrowHealthLbl;
    UITextView* nextdayHealthLbl;
    NSString* polutant;
    UILabel* nextdayBurnlbl;
    UILabel* tomorrowBurnlbl;
    UILabel* todayBurnlbl;
    UIImageView* todayBurnImgView;
    UIImageView* tomorrowBurnImgView;
    UIImageView* nextdayBurnImgView;
    UIImageView* trendArrow;
    UILabel* todayForcastLbl;
    UILabel* tomorrowForcastLbl;
    UILabel* nextdayForcastLbl;
    UIImageView* background;
    UILabel* dateTime;
    NSMutableArray* regionData;
    NSMutableArray* countyData;
    UIImageView* todayHealthIMGView;
    UIImageView* tomorrowHealthIMGView;
    UIImageView* nextdayHealthIMGView;
    Reachability* networkReachability;
    NetworkStatus networkStatus;
    
}

@property (strong, nonatomic) NSMutableArray* favoriteCounties;
@property (strong, nonatomic) UIActivityIndicatorView* indicator;

@property (readonly,nonatomic)UISwipeGestureRecognizer *recognizer_open,*recognizer_close;
@property (readonly,nonatomic) int menuDrawerX, menuDrawerWidth;

-(IBAction)showMenuOptions:(id)sender;

-(IBAction)showFavoritesMenu:(id)sender;

-(void)refreshDATA;

-(void)goToRegions;

-(void)goToTrendChart;

-(void)goToLegend;

-(void)goToAdvisory;

-(void)goToDisclaimer;

-(void)goToAbout;

-(void)handleSwipes:(UISwipeGestureRecognizer*) sender;

-(void)drawAnimation;

@end
