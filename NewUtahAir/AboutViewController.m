//
//  AboutViewController.m
//  NewUtahAir
//
//  Created by Developement on 1/30/15.
//  Copyright (c) 2015 NCAST. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //add backround image
    UIImage* backgrounImage = [UIImage imageNamed:@"background_iphone1.png"];
    UIImageView* background = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [background setImage:backgrounImage];
    [background setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:background];
    
    //add about statement
    UITextView* aboutStatement = [[UITextView alloc]initWithFrame:CGRectMake(25, 0, self.view.frame.size.width-45, self.view.frame.size.height/2)];
    [aboutStatement setUserInteractionEnabled:NO];
    [aboutStatement setBackgroundColor:[UIColor clearColor]];
    [aboutStatement setText:@"The Utah Air application was developed by the National Center for Automotive Science and Technology at Weber State University(NCAST) in partnership with the Utah Division of Air Quality.\n\n NCAST is dedicated to increasing public awareness and involvement of improving air quality through applied research, science, and education so that action can be taken to create and maintain a better way of life.\n\n This application utilizes data provided by the Utah Division of Air Quality, www.airquality.utah.gov"];
    [aboutStatement sizeToFit];
    [aboutStatement setTextColor:[UIColor whiteColor]];
    [self.view addSubview:aboutStatement];
    
    //add title
    self.navigationItem.title = @"About Utah Air";
    
    //add weber state image
    UIImageView* weberLogo = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-((self.view.frame.size.width*0.5)+65), self.view.frame.size.width*0.5, self.view.frame.size.width*0.5)];
    [weberLogo setImage:[UIImage imageNamed:@"sighan"]];
    [weberLogo setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:weberLogo];
    
    //add DAQ image
    UIImageView* DAQLogo = [[UIImageView alloc]initWithFrame:CGRectMake(weberLogo.frame.origin.x+weberLogo.frame.size.width-10, self.view.frame.size.height-((self.view.frame.size.width*0.5)+65), self.view.frame.size.width*0.5, self.view.frame.size.width*0.5)];
    [DAQLogo setImage:[UIImage imageNamed:@"Q_logo2"]];
    [DAQLogo setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:DAQLogo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark orientation controll

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}



@end
